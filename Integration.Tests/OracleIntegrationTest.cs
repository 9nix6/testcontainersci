using Integration.Tests.Fixtures;
using Oracle.ManagedDataAccess.Client;

namespace Integration.Tests;

public class OracleIntegrationTest(OracleContainerFixture fixture) : IClassFixture<OracleContainerFixture>
{
    private readonly OracleContainerFixture _fixture = fixture;

    [Fact]
    public async Task TestSelectOneFromDual()
    {
        // Arrange 

        using var connection = new OracleConnection(_fixture.Container.GetConnectionString());
        using var command = new OracleCommand("SELECT 1 FROM DUAL", connection);
        await connection.OpenAsync();

        // Act

        var result = await command.ExecuteScalarAsync();

        // Assert

        Assert.Equal(1, Convert.ToInt32(result));
    }
}
