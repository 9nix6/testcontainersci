﻿using Testcontainers.Oracle;

namespace Integration.Tests.Fixtures;

public sealed class OracleContainerFixture : IAsyncLifetime
{
    public OracleContainer Container { get; }

    public OracleContainerFixture()
    {
        Container = new OracleBuilder()
            .Build();
    }

    public Task InitializeAsync() => Container.StartAsync();

    public Task DisposeAsync() => Container.StopAsync();
}
