﻿using Testcontainers.MsSql;

namespace Integration.Tests.Fixtures;

public sealed class MsSqlContainerFixture : IAsyncLifetime
{
    public MsSqlContainer Container { get; }

    public MsSqlContainerFixture()
    {
        Container = new MsSqlBuilder()
            .Build();
    }

    public Task InitializeAsync() => Container.StartAsync();

    public Task DisposeAsync() => Container.StopAsync();
}
