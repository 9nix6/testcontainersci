﻿using Integration.Tests.Fixtures;
using Microsoft.Data.SqlClient;

namespace Integration.Tests;

public class MsSqlIntegrationTest(MsSqlContainerFixture fixture) : IClassFixture<MsSqlContainerFixture>
{
    private readonly MsSqlContainerFixture _fixture = fixture;

    [Fact]
    public async Task TestSelectOne()
    {
        // Arrange

        using var connection = new SqlConnection(_fixture.Container.GetConnectionString());
        using var command = new SqlCommand("SELECT 1", connection);
        await connection.OpenAsync();

        // Act

        var result = await command.ExecuteScalarAsync();

        // Assert

        Assert.Equal(1, Convert.ToInt32(result));
    }
}
