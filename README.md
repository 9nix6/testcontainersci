# Testcontainers CI

This project demonstrates how to use [Testcontainers .NET](https://dotnet.testcontainers.org/) in a GitLab CI pipeline. Testcontainers is a .NET library providing lightweight, throwaway instances of common databases, Selenium web browsers, or anything else that can run in a Docker container.

The main component of this project is the `.gitlab-ci.yml` file. This file defines the configuration of the GitLab CI/CD pipeline for runing integration tests with MsSql and Oracle database servers. The integration tests are very simple and are only provided to be executed by the CI pipeline using Testcontaners for MsSql and Oracle.

I've decided to post this repositoy as the Testcontainers website does not provide a working CI pipeline example and many hours have been spent researching and testing the topic.

## .gitlab-ci.yml

[![Pipeline status](https://gitlab.com/9nix6/testcontainersci/badges/main/pipeline.svg)](https://gitlab.com/9nix6/testcontainersci/-/blob/main/.gitlab-ci.yml)

The `.gitlab-ci.yml` file is a YAML file where you configure your GitLab CI/CD pipeline. In this project, it is used to set up the environment for running the integration tests with Testcontainers.

The simple pipeline is divided into different stages, each representing a phase in the CI/CD process. The stages in this project include build, test stages. 

In the test stage, we use Testcontainers to run our integration tests. These tests interact with a real instance of the service that we are testing against, which is running inside a Docker container. This provides a high level of confidence in the correctness of the tests.

After the tests are run, the Docker containers used in the tests are automatically cleaned up by Testcontainers, ensuring that the test environment is always clean and ready for the next run.

For more details on how to use Tescontainers, you can check the simple integration tests in this repository and refer to the [official Testcontainers for .NET website](https://dotnet.testcontainers.org/).

For more details on how to configure your own `.gitlab-ci.yml` file, please refer to the [official GitLab CI/CD pipeline configuration reference](https://docs.gitlab.com/ee/ci/yaml/).